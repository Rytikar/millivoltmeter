# Introduction #

The MilliVoltMeter is a reimplementation of the Arduino software for the [Scullcom MilliVoltMeter](https://www.youtube.com/watch?v=wxCW3pbvRi8&list=PLUMG8JNssPPzb4oVC4RgWHdG144Nne9pq) project.

The original software works just fine, but I wanted to implement some more features and clean up the code a bit.

### Release Versions ###
* 1.0, 16.Oct.2018, first release
* 1.2, 25.May.2019, fixed calibration problem. Added support for high and low active buttons

Please report errors if you find some!

This readme and documentation is under construction...

### Installation ###

* Click on the [Downloads](https://bitbucket.org/Rytikar/millivoltmeter/downloads/) link.
* Click on 'Download repository'
* You will receive a .zip file, containing the repository
* Unzip the file into a folder called 'millivoltmeter'. That is important, as the Arduino IDE needs it to work
* You will end up with lots of files. That's because I have implemented the software with Visual Studio, that's where the .sln and other VS specific files come from. But don't worry...
* Open the MillivoltMeter.ino file in the Arduino IDE as usual.
* Build and upload the project to your Arduino as usual. 
* Have fun

### Documentation ###

The look and feel resembles the original software. Still, I've changed the internals a bit and added some features.

* Implements all of the features available in Louis original software. Some enhancements done.
* All buttons are interrupt controlled, not polled.
* The adc is converting continuously, also controlled by timer interrupt every 10mS. The Low and High values are determined every conversion.
* The sample uses a 'exponential moving average' calculation resulting in a stable display
* The bar graph adapts to the voltage so it is also useful at lower voltages.
* The 'old' Calibrate and Cal-Reset buttons are repurposed to monitor Low and High voltage readings. I had them mounted on the front to begin with.
* As I don't see the need to 'clear' the calibration, this functionality has been removed 
* For the rare need to recalibrate the unit, press BOTH Low and High monitoring buttons simultaniously (the old Calibrate and Cal-Reset).
### Who do I talk to? ###

* dps@rosenkilde-hoppe.dk

### Thanks ###

To:

* [Scullcom](http://www.scullcom.uk/)
* [VisualMicro](https://www.visualmicro.com/)
