// SCULLCOM HOBBY ELECTRONICS
// Millivolt Meter MK2
// MILLIVOLT METER USING LTC2400 24bit ADC CHIP
// Based on software version 33.00 (uses I2C LCD 16x2 Display)
// Uses PCB designed by Greg Christenson [BARBOURI] which is available from OSH Park
// This PCB is available worldwide (free shipping) from OSH Park for just a few dollars. Check their link below for this project:
// https://oshpark.com/shared_projects/qgv0fpKN
// The PCB design file is also available on this webpage to download.
// https://644db4de3505c40a0444-327723bce298e3ff5813fb42baeefbaa.ssl.cf1.rackcdn.com/0fa8729a07e906a84e6cf5762d45a390.brd

// 4.096 volt precision reference (ADR4540)
//LTC2400 SCK to digital pin 13
//LTC2400 SDO to digital pin 12
//LTC2400 CS  to digital pin 10

// --- Library includes -----------------------------------------------------------------------

#include <EEPROM.h>                           // include EEPROM library
#include <SPI.h>                              // Serial Peripheral Interface Library used to communicate with LTC2400 (MISO, MOSI and SCK)
#include <Wire.h>                             // Library allows you to communicate with I2C devices [A4 (SDA), A5 (SCL)]
#include <LiquidCrystal_I2C.h>                // F Malpartida's NewLiquidCrystal library
#include <TimerOne.h>						  // https://github.com/PaulStoffregen/TimerOne	

#define EI_ARDUINO_INTERRUPTED_PIN

#include <EnableInterrupt.h>

#include "MvmConst.h"

// --- Global variables -----------------------------------------------------------------------

#define BTN_ACTIVEHIGH	// High = with debouncing

#ifdef BTN_ACTIVEHIGH
#define BTN_ACTIVE HIGH
#else
#define BTN_ACTIVE LOW
#endif

//Set the pins on the I2C chip used for LCD connections
//ADDRESS,EN,R/W,RS,D4,D5,D6,D7
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7); // 0x27 is the default I2C bus address (this could be different for some modules)

volatile long _adcSample = 0;
volatile long _adcSamples = 0;
volatile long _adcLowSample = MAXLONG;
volatile long _adcHighSample = MINLONG;
volatile bool _adcSampleOverload = false;
volatile bool _adcSamplePositive = false;
volatile SecondaryMode _secondaryMode = SecondaryBar;
volatile bool _noButtonIrqs = false;
volatile byte _numberOfDecimals = 6;
volatile long _lastDisplayedLow = MAXLONG;
volatile long _lastDisplayedHigh = MINLONG;
static float _lastBarVolts = MINLONG;

long _calibrationOffset[CALIBRATIONVALUES] = { 0, 0, 0, 0 };

//--- Battery Status variables -----------------------------------------------------------------------

int analogInput = 0;  // sets analog input to A0 for battery check
int battery = 0;
float vout = 0.0;
float batteryVolts = 0.0;
float R1 = 1000000.0; // resistance of R1 (1M)    // part of resistor divider for battery status check
float R2 = 100000.0; // resistance of R2 (100K)   // part of resistor divider for battery status check

//--- Bar Graph Special Characters Bit Map------------------------------------------------------------

byte p20[8] = { B10000,B10000,B10000,B10000,B10000,B10000,B10000,B10000, };
byte p40[8] = { B11000,B11000,B11000,B11000,B11000,B11000,B11000,B11000, };
byte p60[8] = { B11100,B11100,B11100,B11100,B11100,B11100,B11100,B11100, };
byte p80[8] = { B11110,B11110,B11110,B11110,B11110,B11110,B11110,B11110, };
byte p100[8] = { B11111,B11111,B11111,B11111,B11111,B11111,B11111,B11111, };

// --- Setup function (only runs once) --------------------------------------------------------

void setup(void) 
{
	Serial.begin(9600);                                      //used for testing only

	setupIOPins();
	setupSPIBus();
	setupLCD();
	setupFont();											 //bar graph character bit map setup

	for(byte i = 0; i < CALIBRATIONVALUES; i++)
		_calibrationOffset[i] = EEPROMreadlong(i);

	showIntro();
	batteryStatus();

	enableInterrupt(BUTTON_LOW, buttonIsr, CHANGE);
	enableInterrupt(BUTTON_HIGH, buttonIsr, CHANGE);
	enableInterrupt(BUTTON_DEC, buttonIsr, CHANGE);
	enableInterrupt(BUTTON_HOLD, buttonIsr, CHANGE);
	enableInterrupt(BUTTON_BAR, buttonIsr, CHANGE);

	Timer1.initialize(10000);
	Timer1.attachInterrupt(adcIsr);
	digitalWrite(LTC2400_CS, LOW);							// Setting chips select LOW starts conversion, we check on it in adcIsr()
}

// --- Main Programme loop ------------------------------------------------------------------------------
void loop(void) 
{
	if (digitalRead(BUTTON_LOW) == BTN_ACTIVE && digitalRead(BUTTON_HIGH) == BTN_ACTIVE) calibrationTrimToggled();

	displayReading();                                                            
}

// --- Interrupt routines --------------------------------------------------------------------------------

void buttonIsr()
{
	if (_noButtonIrqs) return;

	static uint32_t lastInterruptTime = 0;
	uint32_t interruptTime = millis();
	uint8_t pin = arduinoInterruptedPin;

#ifdef BTN_ACTIVEHIGH
	if (digitalRead(pin) == BTN_ACTIVE)
#else
	if ((interruptTime - lastInterruptTime > DEBOUNCE_DELAY_MS) && (digitalRead(pin) == BTN_ACTIVE))
#endif
		switch (pin)
		{
		case BUTTON_HIGH: setSecondaryMode(SecondaryHigh); break; 
		case BUTTON_LOW:  setSecondaryMode(SecondaryLow); break;
		case BUTTON_HOLD: setSecondaryMode(SecondaryHold); break;
		case BUTTON_BAR:  setSecondaryMode(SecondaryBar); break;
		case BUTTON_DEC: _numberOfDecimals = _numberOfDecimals < 6 ? _numberOfDecimals + 1 : 0; break;
		}

	lastInterruptTime = interruptTime;
}

void adcIsr()
{
	if (PINB & (1 << 4)) return;				 //check to see if ADC is ready by testing EOC

	Timer1.stop(); 

	uint32_t sample = 0;
	for (int i = 0; i < 4; ++i)                  // Ready 4 bytes (32 bits) from the ADC
	{
		sample <<= 8;                           // Before each reading shift the existing content over to make room
		sample |= SPI.transfer(0xFF);           // Read one byte
	}

	_adcSamplePositive = sample & 0x20000000L;
	_adcSampleOverload = sample & 0x10000000L;

	sample &= 0x0FFFFFFFL;                     // Discard 4 status bits of the first byte
	sample >>= 4;                              // Discard 4 left most sub LSB bits
	if ((_adcSamples == 0) || (abs((long)sample - _adcSample) > 100))
	{
		_adcSample = sample;
		_adcSamples = 1;
	}
	else
	{
		long tmp = (_adcSample * _adcSamples) + sample;
		_adcSample = tmp / ++_adcSamples;
		if (_adcSamples > 1000) _adcSamples = 100;
	}
	if (_adcSample < _adcLowSample) _adcLowSample = _adcSample;
	if (_adcSample > _adcHighSample) _adcHighSample = _adcSample;

	Timer1.start();
}

// --- Various functions -----------------------------------------------------------------------------

char* floatToStr(float value, char* result, int precision = 2, int width = -1, char filler = SPACECHAR)
{
	char str[14];               // Array to contain decimal string
	uint8_t ptr = 0;            // Initialise pointer for array
	uint8_t i = 0;
	int8_t digits = 1;         // Count the digits to avoid array overflow
	float rounding = 0.5;       // Round up down delta

	if (precision > 7) precision = 7;         // Limit the size of decimal portion

	for (uint8_t i = 0; i < precision; ++i) rounding /= 10.0; // Adjust the rounding value

	if (value < -rounding)    // add sign, avoid adding - sign to 0.0!
	{
		str[ptr++] = '-'; // Negative number
		str[ptr] = 0; // Put a null in the array as a precaution
		digits = 0;   // Set digits to 0 to compensate so pointer value can be used later
		value = -value; // Make positive
	}

	value += rounding; // Round up or down

	if (value >= 2147483647) return NULL;

	// No chance of overflow from here on

	// Get integer part
	unsigned long temp = (unsigned long)value;

	// Put integer part into array
	ltoa(temp, str + ptr, 10);

	if (precision > 0)
	{
		// Find out where the null is to get the digit count loaded
		while ((uint8_t)str[ptr] != 0) ptr++; // Move the pointer along
		digits += ptr;                  // Count the digits

		str[ptr++] = '.'; // Add decimal point
		str[ptr] = '0';   // Add a dummy zero
		str[ptr + 1] = 0; // Add a null but don't increment pointer so it can be overwritten

		value -= temp; // Get the decimal portion

					   // Get decimal digits one by one and put in array
					   // Limit digit count so we don't get a false sense of resolution
		while ((i < precision) && (digits < 9)) // while (i < precision) for no limit but array size must be increased
		{
			i++;
			value *= 10;       // for the next decimal
			temp = value;      // get the decimal
			ltoa(temp, str + ptr, 10);
			ptr++; digits++;         // Increment pointer and digits count
			value -= temp;     // Remove that digit
		}
	}
	if (width < 0) width = strlen(str);
	for (i = 0; i < width - strlen(str); i++) result[i] = filler;
	result[i] = 0;

	return strcat(result, str);
}

void setupIOPins(void) 
{
#ifdef BTN_ACTIVEHIGH
	pinMode(BUTTON_HIGH, INPUT);        // set to input with internal pull up resistor
	pinMode(BUTTON_LOW, INPUT);          // set to input with internal pull up resistor
	pinMode(BUTTON_DEC, INPUT);          // set to input with internal pull up resistor
	pinMode(BUTTON_HOLD, INPUT);          // set to input with internal pull up resistor
	pinMode(BUTTON_BAR, INPUT);          // set to input with internal pull up resistor
#else
	pinMode(BUTTON_HIGH, INPUT_PULLUP);        // set to input with internal pull up resistor
	pinMode(BUTTON_LOW, INPUT_PULLUP);          // set to input with internal pull up resistor
	pinMode(BUTTON_DEC, INPUT_PULLUP);          // set to input with internal pull up resistor
	pinMode(BUTTON_HOLD, INPUT_PULLUP);          // set to input with internal pull up resistor
	pinMode(BUTTON_BAR, INPUT_PULLUP);          // set to input with internal pull up resistor
#endif

	pinMode(analogInput, INPUT);                // used for battery status check
	pinMode(LTC2400_CS, OUTPUT);                // set for output
	digitalWrite(LTC2400_CS, HIGH);             // Setting chips select HIGH halts conversion
}

void setupSPIBus(void)
{
	SPI.begin();                                //initialise SPI bus
	SPI.setBitOrder(MSBFIRST);                  //Sets the order of bits shifted out and in to SPI bus, MSBFIRST (most-significant bit first)
	SPI.setDataMode(SPI_MODE0);                 // Mode 0 (MOSI read on rising edge (CPLI=0) and SCK idle low (CPOL=0))
	SPI.setClockDivider(SPI_CLOCK_DIV16);       // Divide Arduino clock by 16 to give a 1 MHz SPI clock
}

void setupLCD(void) 
{
	lcd.begin(16, 2);                           // LCD set for 16 by 2 display
	lcd.setBacklightPin(3, POSITIVE);            // (BL, BL_POL)
	lcd.setBacklight(HIGH);                     // LCD backlight turned ON
}

// --- Convert ADC reading to Decimal Voltage reading -------------------------------------------------------

float convertToVoltage(long sample) 
{
	return sample * 10.0 * VOLTAGE_REFERENCE / 0x1000000;
}

//--- Calculate Voltage reading running average and add zero Calibration and trim Calibration factors ----

long getCalibratedAdcSample(long sample)
{
	if (sample > SAMPLE7p5V) return sample + _calibrationOffset[0];
	else if (sample > SAMPLE3V) return sample + _calibrationOffset[1];
	else if (sample > SAMPLE0p5V) return sample + _calibrationOffset[2];
	else return sample + _calibrationOffset[3];
}

//--- Print Voltage Reading to Display -----------------------------------------------------------------
void displayReading() 
{
	static unsigned long lastMillis = 0;

	unsigned long m = millis();
	if (m < lastMillis) return;
	lastMillis = m + DISPLAY_UPDATE_MS;

	long avg = getCalibratedAdcSample(_adcSample);
	float volts = avg == MAXLONG ? MAXLONG : convertToVoltage(avg);
	displayVolts(volts, 0);
	displaySecondary(volts);
}

void displayVolts(float volts, byte row)
{
	char spc[17] = "                ";
	char tmp[13] = "NEGATIVE";

	lcd.setCursor(0, row);

	if ((volts >= 0) && _adcSamplePositive)
	{
		floatToStr(volts < 0.001 ? volts * 1000000 : (volts < 1 ? volts * 1000 : volts), tmp, _numberOfDecimals);
		strcat(tmp, volts < 0.001 ? MICROSTR : (volts < 1 ? MILLISTR : VOLTSSTR));
	}

	spc[16 - strlen(tmp)] = '\0';
	lcd.print(tmp);
	lcd.print(spc);
}

void displaySecondary(float volts)
{
	static SecondaryMode lastSecondaryMode = SecondaryOff;
	char spc[17] = "                ";

	switch (_secondaryMode)
	{
	case SecondaryOff: if (lastSecondaryMode != SecondaryOff) clearRow(1); break;
	case SecondaryHold:
	{
		if (lastSecondaryMode == SecondaryHold) return;
		displayVolts(volts, 1);
		printAt(14, 1, "Hd");
		break;
	}
	case SecondaryLow:
	{
		long lowSample = _adcLowSample;
		if (_lastDisplayedLow == lowSample) return;
		_lastDisplayedLow = lowSample;
		displayVolts(convertToVoltage(getCalibratedAdcSample(lowSample)), 1);
		printAt(14, 1, "Lo");
		break;
	}
	case SecondaryHigh:
	{
		long highSample = _adcHighSample;
		if (_lastDisplayedHigh == highSample) return;
		_lastDisplayedHigh = highSample;
		displayVolts(convertToVoltage(getCalibratedAdcSample(highSample)), 1);
		printAt(14, 1, "Hi");
		break;
	}
	case SecondaryBar:
		{
		    float barVolts = volts;
			while (barVolts > 0 && barVolts < 1) barVolts *= 10;
			if (abs(barVolts - _lastBarVolts) < 0.1) return;               // only refresh bar graph if voltage changed (stops ficker)
			_lastBarVolts = barVolts;

			if (barVolts > 16.0) barVolts = 16;
			int	volt1 = (int)barVolts;                                 // voltage reading before the decimal point
			float volt2 = barVolts - volt1;                            // voltage reading after the decimal point

			lcd.setCursor(0, 1);

			for (int i = 0; i < (volt1); ++i) lcd.write(5);
			if (volt2 > 0.9) lcd.write(5);
			else if (volt2 > 0.7) lcd.write(4);
			else if (volt2 > 0.5) lcd.write(3);
			else if (volt2 > 0.3) lcd.write(2);
			else if (volt2 > 0.1) lcd.write(1);
			else lcd.write(' ');
			spc[16 - (volt1 + 1)] = '\0';
			lcd.print(spc);
	}
	}
	lastSecondaryMode = _secondaryMode;
}

//---Intro Screen at Switch On -----------------------------------------------------------------
void showIntro(void) 
{
	lcd.clear();
	printAt(4, 0, "SCULLCOM");
	printAt(0, 1, "Hobby Electronic");
	delay(INTRO_DELAY_MS);
	lcd.clear();
	printAt(0, 0, "Millivolt Meter");
	printAt(0, 1, "Software V1.2rrh");
	delay(INTRO_DELAY_MS);
	lcd.clear();
}

//--- Show Calibration Data -----------------------------------------------------------------
void showCalibrationData(long cal) 
{
	lcd.clear();
	printAt(0, 0, "Cal. Data = ");
	lcd.print(cal);
	delay(CALIBRATION_PROMPT_DELAY_MS);
	lcd.clear();
}

//--- Bar Graph Characters Define ----------------------------------------------------------
void setupFont(void) 
{
	lcd.createChar(1, p20);
	lcd.createChar(2, p40);
	lcd.createChar(3, p60);
	lcd.createChar(4, p80);
	lcd.createChar(5, p100);
}

//--- Battery Voltage Status Routine --------------------------------------------------------
void batteryStatus(void) 
{
	lcd.clear();
	battery = analogRead(analogInput);         // read the value at analog input
	vout = (battery * 5.0) / 1024.0;            
	batteryVolts = vout / (R2 / (R1 + R2));
	printAt(1, 0, "Battery Status");
	lcd.setCursor(3, 1);
	lcd.print(batteryVolts, 2);
	lcd.print(" Volts");
	delay(INTRO_DELAY_MS);
	lcd.clear();
}

//----Calculate Calibration Value and save in eeprom -------------------------------------------------------------------------
void calibrationTrimToggled(void)
{
	_noButtonIrqs = true;

	for(byte i = 0; i < CALIBRATIONVALUES; i++)
	{
		_calibrationOffset[i] = trim(CALIBRATIONVOLT[i], CALIBRATIONSAMPLE[i]);
		EEPROMwritelong(i, _calibrationOffset[i]);
	}

	_secondaryMode = SecondaryOff;
	_noButtonIrqs = false;
}

long trim(const float ref, const long cmp)
{
	lcd.clear();
	printAt(0, 0, "Apply ");
	lcd.print(ref, 1);
	lcd.print("V Ref");
	delay(3000);
	printAt(0, 1, "Press low button");

	while (digitalRead(BUTTON_LOW) != BTN_ACTIVE) {}

	long cal = cmp - _adcSample;
	showCalibrationData(cal);
	return cal;
}

//--- EEPROM routine used for saving Calibration Data -------------------------------------

void EEPROMwritelong(byte idx, long value)
{
	EEPROM.put(EEPROMADDRESS + (idx * sizeof(value)), value);
}

// routine to read back 4 bytes and return with (32 bit) long as value
long EEPROMreadlong(byte idx)
{
	long l;
	EEPROM.get(EEPROMADDRESS + (idx * sizeof(l)), l);
	return l;
}

void clearRow(byte row)
{
	printAt(0, row, "                ");
}

SecondaryMode setSecondaryMode(SecondaryMode secondaryMode)
{
	if (_secondaryMode == secondaryMode) return _secondaryMode = SecondaryOff;
	if (secondaryMode == SecondaryLow) _adcLowSample = _adcSample;
	else if (secondaryMode == SecondaryHigh) _adcHighSample = _adcSample;
	else if (secondaryMode == SecondaryBar) _lastBarVolts = MINLONG;
	return _secondaryMode = secondaryMode;
}

void printAt(const byte col, const byte row, const char* s)
{
	lcd.setCursor(col, row);
	lcd.print(s);
}