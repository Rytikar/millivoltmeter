#pragma once

typedef enum SecondaryMode { SecondaryOff, SecondaryBar, SecondaryHold, SecondaryLow, SecondaryHigh };

// --- PIN assignments ------------------------------------------------------------------------

const byte LTC2400_CS = 10;                          // chip select pin for ADC
const byte BUTTON_HIGH = 2;                        // D2 used for Clear Calibration Memory button
const byte BUTTON_LOW = 3;                        // D3 used for Calibration button
const byte BUTTON_DEC = 4;                        // D4 used for Number of Decimal Places button
const byte BUTTON_HOLD = 5;                        // D5 used for Hold Current Reading on display
const byte BUTTON_BAR = 6;                        // D6 used to toggle Bar Graph Voltage Display (max 16 volt)

// --- Constants ------------------------------------------------------------------------------

const float VOLTAGE_REFERENCE = 4.096F;			// voltage reference used for LTC2400
const int DISPLAY_UPDATE_MS = 100;				// value determines voltage reading update rate (1000 will give a slow update option)
const int DEBOUNCE_DELAY_MS = 100;				// value determines button debounce delay was 150
const int INTRO_DELAY_MS = 2000;				// intro screen delays
const int CALIBRATION_PROMPT_DELAY_MS = 2000;   // calibration prompt delay time

const long SAMPLE10V = 4096000;
const long SAMPLE7p5V = 3072000;
const long SAMPLE5V = 2048000;
const long SAMPLE3V = 1228800;
const long SAMPLE1V = 409600;
const long SAMPLE0p5V = 204800;
const long SAMPLE0p1V = 40960;
const long SAMPLE0p05V = 20480;
const long SAMPLE0V = 0;

const byte CALIBRATIONVALUES = 4;
const float CALIBRATIONVOLT[CALIBRATIONVALUES] = { 10, 5, 1, 0.1};
const long CALIBRATIONSAMPLE[CALIBRATIONVALUES] = { SAMPLE10V, SAMPLE5V, SAMPLE1V, SAMPLE0p1V};
const long CALIBRATIONTHRESHOLD[CALIBRATIONVALUES] = { SAMPLE10V , SAMPLE5V, SAMPLE1V, SAMPLE0p1V};

const char SPACECHAR = ' ';
const char MICROSTR[] = {SPACECHAR, 228, 'V', 0};
const char MILLISTR[] = {SPACECHAR, 'm', 'V', 0 };
const char VOLTSSTR[] = {SPACECHAR, 'V', 0};
const long MAXLONG = 2147483647L;
const long MINLONG = -2147483648L;

const int EEPROMADDRESS = 0;
